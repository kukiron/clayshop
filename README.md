# ClayShop React App

ClayShop react app uses `https://api.clayshop.herokuapp.com` for the REST API endpoints. This app implements a web interface for a smart-lock screen.

Check the [Demo](https://clayshop.herokuapp.com).

## Running the app

To build the client side application, clone the repo & run `cd client && npm install` from the root directory.

Then, run `npm run dev:client` for the development build & go to `http://localhost:8080`
